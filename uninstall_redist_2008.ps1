# This script removes Microsoft Visual C++ 2008 Redistributable version 9.0.30729 and installs 9.0.30729.4188
# Owner: S.kahlon@F5
# ENH46565 | VUL0000697
# Variables
$appversion = "9.0.30729"
$vredistver = Get-WmiObject -Class Win32_Product -Filter "Name LIKE 'Microsoft Visual C++ 2008 Redistributable - x86%'"  | Select-Object -expand version
$vredistapp = Get-WmiObject -Class Win32_Product -Filter "Name LIKE 'Microsoft Visual C++ 2008 Redistributable - x86%'" | Where-object version -eq "9.0.30729"
$installdir = "c:\temp\updatevc"
$exepath = "$installdir\vcredist_x86_4148.exe"
$url = "https://download.microsoft.com/download/9/7/7/977B481A-7BA6-4E30-AC40-ED51EB2028F2/vcredist_x86.exe"
$outpath = "$installdir/vcredist_x86_4148.exe"
$logfile = "$installdir/vcredist_log.txt"
# Start logging
Start-transcript -Path $logfile -Append
# Uninstall old version 9.0.30729
$vredistver
if ($vredistver -eq "9.0.30729") {
    Write-Host "Visual Studio Redistributable package version $appversion found. Removing ..."
    $vredistapp | foreach-object -process { $vredistapp.Uninstall() }
    Write-Host "Starting component cleanup . . ."
    Dism.exe /online /Cleanup-Image /StartComponentCleanup
    #Install new version . Visual C++ 2008 Redistributable version 9.0.30729.4148
    New-Item -ItemType directory -Path "$installdir" -Force -Verbose
    Invoke-WebRequest -Uri $url -OutFile $outpath -Verbose
    Start-Process $exepath -ArgumentList "/q" -Wait -Verbose
    if ($LASTEXITCODE -eq "0" ) {
        Write-Host  "`n9.0.30729.4148 Installed!" -Foregroundcolor Green
    }
}
else {
    Write-Host "Visual Studio Redistributable package version $appversion NOT found. No action taken"
}
#End Logging
Stop-Transcript

